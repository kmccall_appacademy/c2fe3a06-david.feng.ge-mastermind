class Code
  attr_reader :pegs

  PEGS = {
    "r" => "red",
    "g" => "green",
    "b" => "blue",
    "y" => "yellow",
    "o" => "orange",
    "p" => "purple"
  }
    # r: "red",
    # g: "green",
    # b: "blue",
    # y: "yellow",
    # o: "orange",
    # p: "purple"


  def self.parse(string)
    pegs_array = []
    string.downcase.chars do |peg|
      if valid?(peg)
        pegs_array << peg
      else
        raise Exception("invalid input")
      end
    end

    Code.new(pegs_array)
  end

  def self.random #class method: Code::random
    random_pegs = []
    0.upto(3).each do |index|
      random_pegs[index] = PEGS.keys.sample
    end

    Code.new(random_pegs)
  end

  def initialize(input_pegs)
    @pegs = input_pegs
  end

  def [](number)
    @pegs[number]
  end

  def exact_matches(other_code)
    0.upto(3).count { |index| self[index] == other_code[index] }
  end

  def near_matches(other_code)
    near_matches_arrays(@pegs, other_code.pegs)
  end

  def ==(other_code)
    return false if other_code.class != Code
    0.upto(3).all? { |index| self[index] == other_code[index] }
  end

  private

  def self.valid?(peg)
    PEGS.keys.include?(peg)
  end

  def near_matches_arrays(array1, array2)

    near_match_count = 0
    near_matched_index2 = []
    0.upto(3).each do |index1|
      0.upto(3).each do |index2|
        if array1[index1] == array2[index2]
          if index1 == index2
            next
          elsif !near_matched_index2.include?(index2)
            near_matched_index2 << index2
            near_match_count += 1
          end
        end
      end

    end
    near_match_count

  end

end

class Game
  attr_reader :secret_code
end
